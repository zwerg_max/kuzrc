# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit rpm

DESCRIPTION="Chromium with GOST support"
HOMEPAGE="https://github.com/deemru/chromium-gost"

KEYWORDS="amd64"

SRC_URI="amd64? ( https://github.com/deemru/${PN}/releases/download/${PV}/${P}-linux-amd64.rpm )"

SLOT="0"
RESTRICT="strip mirror"
LICENSE="GPL-3"
IUSE=""

NATIVE_DEPEND=""

RDEPEND="
	${NATIVE_DEPEND}
"
DEPEND="${RDEPEND}"

S="${WORKDIR}"

src_unpack() {
	rpm_src_unpack ${A}
}

src_install() {
	cp -vR "${S}"/* "${D}"/
}
